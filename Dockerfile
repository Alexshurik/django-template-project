FROM python:3.6
ENV PYTHONUNBUFFERED 1

RUN mkdir /home/app \
 && mkdir /home/app/docs \
 && mkdir -p /var/logs/ \
 && mkdir -p /var/untitled/ \
 && mkdir -p /var/untitled/logs/ \
 && mkdir -p /var/untitled/static/ \
 && mkdir -p /var/untitled/media/
WORKDIR /home/app

ADD docs/requirements.txt /docs/
RUN pip install -r /docs/requirements.txt

COPY . .
ENTRYPOINT ["bin/docker/wait-for-postgres-start.sh"]