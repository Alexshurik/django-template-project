import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

DEBUG = False

ROOT_URLCONF = 'untitled.urls'

WSGI_APPLICATION = 'untitled.wsgi.application'

ADMINS = [
    ('untitled', 'untitled@mail.ru'),
]

MEDIA_URL = '/media/'
MEDIA_ROOT = '/var/untitled/media/'

STATIC_URL = '/static/'
STATIC_ROOT = '/var/untitled/static/'

SECRET_KEY = 'dont use me in production))'
