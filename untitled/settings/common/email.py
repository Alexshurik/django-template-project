try:
    from untitled.settings.enviroment import sensitive_data
except:
    raise ImportError('Отсутствует файл "sensitive_data". См. пример в settings/enviroment/sensitive_data.example')

# Предпочительнее такое использование вместо
# from untitled.settings.senstivie_data import *
# Т.к. так легче понять, какой настройки не хватает на этапе запуска проекта
EMAIL_USE_TLS = sensitive_data.EMAIL_USE_TLS
EMAIL_HOST = sensitive_data.EMAIL_HOST
EMAIL_HOST_USER = sensitive_data.EMAIL_HOST_USER
EMAIL_HOST_PASSWORD = sensitive_data.EMAIL_HOST_PASSWORD
EMAIL_PORT = sensitive_data.EMAIL_PORT
