from os import environ

# Импорт общих настроек
from .common import *


USE_SENTRY = False

# Импорт настроек, специфичных для окружения
environ.setdefault('DJANGO_ENVIROMENT_SETTINGS', 'local_dev')
ENV = environ['DJANGO_ENVIROMENT_SETTINGS']

if ENV == 'docker_dev':
    from .enviroment.docker_dev import *
elif ENV == 'docker_prod':
    from .enviroment.docker_prod import *
else:
    from .enviroment.local_dev import *

# Импорт локальных настроек (см. local.example.py)
try:
    from untitled.settings.enviroment.local import *
except:
    pass

if USE_SENTRY:
    try:
        from .enviroment import sensitive_data
    except:
        raise ImportError('Отсутствует файл "sensitive_data". См. пример в sensitive_data.example')

    INSTALLED_APPS.append('raven.contrib.django.raven_compat')
    RAVEN_CONFIG = {
        'dsn': sensitive_data.SENTRY_DSN,
    }

if DEBUG:
    # Блок настроек debug_toolbar
    DEBUG_TOOLBAR_PATCH_SETTINGS = False
    INTERNAL_IPS = ['127.0.0.1', ]
    # Если вставить до django.contrib.staticfile, то будет ошибка
    INSTALLED_APPS.insert(0, 'debug_toolbar')
    # Если вставить это в конец middleware, то будет ошибка. Хз поч
    MIDDLEWARE.insert(0, 'debug_toolbar.middleware.DebugToolbarMiddleware')

    def show_on_api(request):
        if request.path.startswith('/api/'):
            return True
        else:
            return False


    DEBUG_TOOLBAR_CONFIG = {
        'SHOW_TOOLBAR_CALLBACK': show_on_api,
    }
