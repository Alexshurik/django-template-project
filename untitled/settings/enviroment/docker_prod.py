try:
    from untitled.settings.enviroment import sensitive_data
except Exception as e:
    print(e)
    print('jopa')
    raise ImportError('Отсутствует файл "sensitive_data". См11. пример в settings/enviroment/sensitive_data.example')

# ToDo: настройки ниже под проект надо менять
ALLOWED_HOSTS = [
    '127.0.0.1',
    '0.0.0.0',
    'untitled.com',
]

DEBUG = False

USE_SENTRY = True

SECRET_KEY = sensitive_data.PROD_SECRET_KEY

DATABASES = sensitive_data.PROD_DATABASES

# ToDo: поменять
BROKER_URL = 'amqp://rabbit'

CORS_ORIGIN_ALLOW_ALL = True
