# Пример файла настроек приватных данных
# Таких, как: ключи от соц. сетей, данные от почтового сервиса и т.д.

# НЕ ЗАЛИВАЕТСЯ В GIT
# ОБЯЗАТЕЛЬНЫЙ ФАЙЛ

# Общее (ключи. от соц. сетей, почтового сервиса)
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.yandex.ru'
EMAIL_HOST_USER = 'untitled@yandex.ru'
EMAIL_HOST_PASSWORD = 'untitled'
EMAIL_PORT = 465

# Production-only данные БД, SECRET_KEY и т.д.
PROD_SECRET_KEY = 'change me'
PROD_DATABASES = {}
SENTRY_DSN = ''
