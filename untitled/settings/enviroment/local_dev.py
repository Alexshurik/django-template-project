DEBUG = True

USE_SENTRY = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'untitled',
        'USER': 'untitled',
        'PASSWORD': 'untitled',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}

ALLOWED_HOSTS = [
    '*'
]

BROKER_URL = 'amqp://rabbit'

CORS_ORIGIN_ALLOW_ALL = True
