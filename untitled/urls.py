from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from rest_framework.documentation import include_docs_urls


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'api/v1/', include('untitled.urls_v1', namespace='api-v1')),
]

if settings.DEBUG:
    # Подключаем django_debug_toolbar и документацию
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
        url(r'^api/docs/', include_docs_urls(title='Untitled')),
    ] + urlpatterns

    # Подключаем раздачу статик файлов для dev версии
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
