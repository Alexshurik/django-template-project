#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset
set -o xtrace

celery worker -A untitled.celery --schedule /var/untitled/worker.db
