#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset
set -o xtrace

celery beat -A untitled.celery --pidfile=/tmp/myprojectbeat.pid -s /var/untitled/celerybeat-schedule
