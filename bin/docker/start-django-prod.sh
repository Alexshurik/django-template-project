#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset
set -o xtrace

/home/app/manage.py collectstatic --noinput
/home/app/manage.py migrate
gunicorn untitled.wsgi:application -c /home/app/etc/gunicorn.conf.py --log-level=info --access-logfile - --error-log -