#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset
set -o xtrace

docker run -t --rm -v untitled_certs:/etc/letsencrypt -v untitled_certs-data:/data/letsencrypt/ -v /var/log/letsencrypt:/var/log/letsencrypt deliverous/certbot renew --webroot --webroot-path=/data/letsencrypt
docker kill -s HUP nginx >/dev/null 2>&1
